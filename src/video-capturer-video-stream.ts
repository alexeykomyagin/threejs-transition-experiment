export class VideoCapturerVideoStream {
  private mediaRecorder: MediaRecorder;

  private chunks: Blob[] = [];
  constructor(private canvas: HTMLCanvasElement, private onVideoReady: (url: string) => void) {

  }

  start() {
    let videoStream = this.canvas.captureStream(30);
    this.mediaRecorder = new MediaRecorder(videoStream);

    this.mediaRecorder.ondataavailable = e => {
      this.chunks.push(e.data);
    };

    this.mediaRecorder.onstop = () => {
      let blob = new Blob(this.chunks, { 'type' : 'video/mp4' }); // other types are available such as 'video/webm' for instance, see the doc for more info
      this.chunks = [];
      let videoURL = URL.createObjectURL(blob);
      this.onVideoReady(videoURL);
    };
    this.mediaRecorder.start()
  }


  stop() {
    this.mediaRecorder?.stop()
  }

}
