import './style.css'

import {
  Color,
  Mesh,
  OrthographicCamera,
  PlaneGeometry,
  Scene, ShaderMaterial, TextureLoader,
  WebGLRenderer,
} from 'three';

import img1 from './assets/images/AlexChad_500.jpg';
import img2 from './assets/images/AlexChad_513.jpg';
import img3 from './assets/images/AlexChad_105.jpg';
import img4 from './assets/images/AlexChad_106.jpg';
import img5 from './assets/images/AlexChad_150.jpg';

import fragmentShader from './shaders/fragment.glsl?raw'
import vertexShader from './shaders/vertex.glsl?raw'
import {FramesCapturerVideoStream} from './frames-capturer-video-stream';

// Get a reference to the container element that will hold our scene
const container = document.querySelector('#scene-container');

// create a Scene
const scene = new Scene();

// Set the background color
scene.background = new Color('skyblue');

// Create a camera
const frustumSize = 1;

const camera = new OrthographicCamera(frustumSize / -2, frustumSize / 2, frustumSize / 2, frustumSize / -2, -10, 10);

// every object is initially created at ( 0, 0, 0 )
// move the camera back so we can view the scene
camera.position.set(0, 0, 10);

// create a geometry
const geometry = new PlaneGeometry(2, 2, 2);

// create a default (white) Basic material
const images = [
  new TextureLoader().load(img1),
  new TextureLoader().load(img2),
  new TextureLoader().load(img3),
  new TextureLoader().load(img4),
  new TextureLoader().load(img5),
]
let fromIndex = 0;
const material = new ShaderMaterial({
  uniforms: {
    uFrom: { value: images[fromIndex]},
    uTo: { value: images[fromIndex + 1]},
    uTime: { value: 0 }
  },
  vertexShader,
  fragmentShader
});
fromIndex++;

// create a Mesh containing the geometry and material
const plane = new Mesh(geometry, material);

// add the mesh to the scene
scene.add(plane);

// create the renderer
const renderer = new WebGLRenderer();

// next, set the renderer to the same size as our container element
renderer.setSize(container.clientWidth, container.clientHeight);

// finally, set the pixel ratio so that our scene will look good on HiDPI displays
renderer.setPixelRatio(window.devicePixelRatio);

// add the automatically created <canvas> element to the page
container.append(renderer.domElement);

const imgShowTime = 5;
let imageShowingTime = 0;
let canvas = renderer.domElement;
// let framesCapturer = new FramesCapturerReadPixels(canvas);
// let framesCapturer = new FramesCapturerCanvasToBlob(canvas);
let framesCapturer = new FramesCapturerVideoStream(canvas, videoURL => {
  let video = document.getElementById('video-player') as HTMLVideoElement;
  video.src = videoURL;
});
// let videoCapturer = new  VideoCapturerVideoStream(canvas, videoURL => {
//   let video = document.getElementById('video-player') as HTMLVideoElement;
//   video.src = videoURL;
// })

framesCapturer.enable()

renderer.setAnimationLoop(() => {
  renderer.render(scene, camera);
  framesCapturer.capture();
  imageShowingTime = material.uniforms.uTime.value += 0.02 ;

  if (imageShowingTime > imgShowTime && fromIndex < images.length) {
    imageShowingTime = material.uniforms.uTime.value = 0;
    material.uniforms.uFrom.value = images[fromIndex];
    material.uniforms.uTo.value = images[fromIndex + 1];
    fromIndex++;
  }

});
// videoCapturer.start();
// setTimeout(() => videoCapturer.stop(), 21000)
setTimeout(() => {
  framesCapturer.disable();
  // let videoURL = framesCapturer.makeVideo();
  // let video = document.getElementById('video-player') as HTMLVideoElement;
  // video.src = videoURL;
}, 20000)

