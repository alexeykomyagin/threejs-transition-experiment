export class FramesCapturerCanvasToBlob {
  private enabled = false;

  frames: Blob[] = [];

  constructor(private readonly canvas: HTMLCanvasElement) {
  }

  enable() {
    this.enabled = true;
  }

  disable() {
    this.enabled = false;
  }

  capture() {
    if (!this.enabled) return;
    this.canvas.toBlob((blob) => {
      this.frames.push(blob)
    })
  }

  makeVideo() {
    var blob = new Blob(this.frames, {type: "video/webm" });
    return URL.createObjectURL(blob);
  }
}
