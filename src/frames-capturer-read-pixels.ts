export class FramesCapturerReadPixels {
  private gl: WebGL2RenderingContext;
  private enabled = false;

  frames: Uint8Array[] = [];

  constructor(private readonly canvas: HTMLCanvasElement) {
    this.gl = this.canvas.getContext('webgl2');
  }

  enable() {
    this.enabled = true;
  }

  disable() {
    this.enabled = false;
  }

  capture() {
    if (!this.enabled) return;
    let pixels = new Uint8Array(this.gl.drawingBufferWidth * this.gl.drawingBufferHeight * 4);
    this.gl.readPixels(0, 0, this.gl.drawingBufferWidth, this.gl.drawingBufferHeight, this.gl.RGBA, this.gl.UNSIGNED_BYTE, pixels);
    this.frames.push(pixels);
  }

  makeVideo() {
    let blob = new Blob(this.frames, { 'type' : 'video/mp4' });
    return URL.createObjectURL(blob);
  }

  makeImage() {
    return URL.createObjectURL(
      new Blob([this.frames[1000]], { type: 'image/png' } /* (1) */)
    );
  }
}
