export class FramesCapturerVideoStream {
  private videoStream: MediaStream;
  private mediaRecorder: MediaRecorder;
  private chunks: Blob[] = [];
  constructor(private canvas: HTMLCanvasElement, private onVideoReady: (url: string) => void) {

  }

  enable() {

    this.videoStream = this.canvas.captureStream(0);
    this.mediaRecorder = new MediaRecorder(this.videoStream);

    this.mediaRecorder.ondataavailable = e => {
      this.chunks.push(e.data);
    };

    this.mediaRecorder.onstop = () => {
      let blob = new Blob(this.chunks, { 'type' : 'video/mp4' }); // other types are available such as 'video/webm' for instance, see the doc for more info
      this.chunks = [];
      let videoURL = URL.createObjectURL(blob);
      this.onVideoReady(videoURL);
    };
    this.mediaRecorder.start()
  }

  capture() {
    // @ts-ignore
    this.videoStream.getVideoTracks()[0].requestFrame();
  }


  disable() {
    this.mediaRecorder?.stop()
  }

}
