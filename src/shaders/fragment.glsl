precision highp float;

uniform sampler2D uFrom;
uniform sampler2D uTo;
uniform float uTime;
varying vec2 vUv;


mat2 rotate(float a) {
    float s = sin(a);
    float c = cos(a);
    return mat2(c, -s, s, c);
}

void main() {
    float time = abs(sin(uTime));
    vec2 uvDevided = fract(vUv * vec2(80., 1.));
    vec2 uvDisplaced1 = vUv + rotate(3.141592/4.) * uvDevided * time * 0.05;
    vec2 uvDisplaced2 = vUv + rotate(3.141592/4.) * uvDevided * (1. - time) * 0.05;

    if (uTime <= 1.5) {
        vec4 from = texture2D(uFrom, uvDisplaced1);
        vec4 to = texture2D(uTo, uvDisplaced2);

        gl_FragColor = mix(from, to, time);
    } else {
        gl_FragColor = texture2D(uTo, vUv);
    }

}
